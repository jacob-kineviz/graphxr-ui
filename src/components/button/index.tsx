import React from 'react';

export interface ButtonProps {
  kind?: 'background' | 'color' | 'text';
}
// const prefixCls = 'graphxr-button';
export interface ButtonState {}

class Button extends React.Component<ButtonProps, ButtonState> {
  public constructor(props: ButtonProps) {
    super(props);
  }

  render(): React.ReactNode {
      return <button>ddd</button>
  }
}

export { Button };
