import React from 'react';
import { Popover, Button, Input, Row, Col } from 'antd';
import 'antd/dist/antd.css';
import './style';
import '@zhanghongbo/graphxr-icons/fonts/icons.css';
import configs from '@zhanghongbo/graphxr-icons';

export interface IconPickerProps {
  iconPrefix?: string;
  hostContainer: React.ReactNode;
  onSelectIcon?: (icon: IconConfig) => void;
}

export type IconConfig = {
  name: string;
  cagtegory: string;
  unicode: string;
};
// const prefixCls = 'graphxr-button';
export interface ButtonState {
  icons: IconConfig[];
  currentCategory: string;
  currentIconName: string;
}

const noIcon = configs.filter((icon: IconConfig) => icon.name == 'no-icon');
configs.splice(configs.indexOf(noIcon[0]), 1); //remove no-icon
class IconPicker extends React.Component<IconPickerProps, ButtonState> {
  public constructor(props: IconPickerProps) {
    super(props);
    this.state = {
      icons: configs.slice(0, 63),
      currentCategory: '',
      currentIconName: '',
    };
  }

  onSelectIcon(icon: IconConfig) {
    const { onSelectIcon } = this.props;
    if (onSelectIcon) {
      onSelectIcon(icon);
    }
  }

  drawIcons(iconConfigs: any[]) {
    const maxIconConfigs = noIcon.concat(iconConfigs);
    return Array.from(Array(8).keys()).map((row) => {
      return (
        <Row key={row} gutter={[16, 16]}>
          {Array.from(Array(8).keys()).map((column) => {
            const iconIndex = row * 8 + column;
            const config = maxIconConfigs[iconIndex];
            const iconName = config && config.name ? `graphxr-${config.name}` : '';
            return (
              <Col key={column} span={3}>
                <i onClick={() => this.onSelectIcon(config)} className={iconName} />
              </Col>
            );
          })}
        </Row>
      );
    });
  }

  onSearch(event: any) {
    const search = event.target.value;
    const fitleredIcons = configs.filter((icon: IconConfig) => {
      const regex = new RegExp(search);
      return regex.test(icon.name);
    });
    this.setState({ icons: fitleredIcons });
  }

  render(): React.ReactNode {
    const { icons } = this.state;
    const { hostContainer } = this.props;
    const searchIcon = <i className="graphxr-search" />;
    const content = (
      <div className="graphxr-icon-picker">
        <Input onChange={this.onSearch.bind(this)} prefix={searchIcon} />
        <div style={{ minHeight: '300px', minWidth: '420px', marginTop: '20px' }}>
          {this.drawIcons(icons)}
        </div>
      </div>
    );
    return (
      <div>
        <Popover placement="left" title={''} content={content} trigger="click">
          {hostContainer}
        </Popover>
      </div>
    );
  }
}

export { IconPicker };
