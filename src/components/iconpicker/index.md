---
title: Icon Picker
nav:
  title: Components
  order: 2
group:
  title: Components
  order: 1
---

# Icon Picker

Demostrate how to use icon picker

## Demo

### Basic Usage

<code src="./demo/basic.tsx"></code>