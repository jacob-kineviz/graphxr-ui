import React from 'react';
import {Button} from 'antd'
// import '@zhanghongbo/graphxr-icons/fonts/icons.css';
import '../style';
import { IconPicker,IconConfig } from '../index';

export default class Test extends React.Component<{},{currentIcon: any}> {
   public constructor(props: any){
	   super(props);
	   this.state = {currentIcon:""}
   }

   onSelectIcon(icon: IconConfig){
	   console.log(icon)
	   this.setState({currentIcon:icon.name})
   }

  render(): React.ReactNode {
	  const {currentIcon} = this.state
    const hostContainer = <Button>{currentIcon?<i className={"graphxr-" + currentIcon} />:"Icon"}</Button>
    return <IconPicker onSelectIcon={this.onSelectIcon.bind(this)} hostContainer={hostContainer} />;
  }
}
